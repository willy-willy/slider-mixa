(function(){
	'use strict';
	function Carousel(element) {
		this.el = document.querySelector(element);
		this.position = 0; // view's position
		this.init();
	}

	Carousel.prototype = {
		init: function() {
			this.wrapper = this.el.querySelector('.carousel-wrapper');	// it is moved
			
			// visible width without left padding of every first item in block
			this.slideWidth = this.el.clientWidth - parseInt(getComputedStyle(this.el).paddingLeft);
			
			// initial amount of slides (blocks)
			this.slideCounter = this.wrapper.clientWidth / this.slideWidth;

			// left and right buttons
			this.el.moveLeft = this.el.querySelectorAll('.arrow')[0];
			this.el.moveRight = this.el.querySelectorAll('.arrow')[1];

			this.currentSlide = 1;	// current slide (block)

			// initial css class for left arrow
			this.el.moveLeft.classList.toggle('btn-disabled');

			this.navigate();
		},
		
		navigate: function() {
			// let self = this;

			// get function that prevents frequently clicks to left
			let preventClickLeft = this.preventClick(this.moveLeft, 500, this);

			// get function that prevents frequently clicks to right
			let preventClickRight = this.preventClick(this.moveRight, 500, this);			

			this.el.moveRight.addEventListener('click', preventClickRight);

			this.el.moveLeft.addEventListener('click', preventClickLeft);

			// when window is resized, get new parametres of carousel (bad variant)
			// window.addEventListener('resize', function() {
			// 	self.slideWidth = self.el.clientWidth - parseInt(getComputedStyle(self.el).paddingLeft);
			// 	self.slideCounter = self.wrapper.clientWidth / self.slideWidth;

			// 	console.log([self.slideWidth, self.slideCounter]);
			// });
		},

		moveRight: function() {
			if (this.currentSlide != this.slideCounter) {
				this.position += this.slideWidth;		
				this.wrapper.style.left = '-' + this.position + 'px';
				this.currentSlide++;

				this.el.moveLeft.classList.remove('btn-disabled');

				if (this.currentSlide == this.slideCounter)
					this.el.moveRight.classList.add('btn-disabled');
			}
		},

		moveLeft: function() {
			if (this.position >= this.slideWidth) {
				this.position -= this.slideWidth;
				this.wrapper.style.left = '-' + this.position + 'px';
				this.currentSlide--;

				// reset disable style from right arrow
				this.el.moveRight.classList.remove('btn-disabled');

				// if current slide is number one then disable left arrow
				if (this.currentSlide == 1) 
					this.el.moveLeft.classList.add('btn-disabled');
			}
		},

		// function wrapper for prevention clicks more frequently than @ms ms
		preventClick: function(func, ms, context) {
			// how much time went from last click
			let timeStamp = 1; // initial value 1 - ready for first click
			
			return function() {
				if (timeStamp) {
				// call function
				func.apply(context, arguments);

				// reset timeStamp to null after cilck
				timeStamp = null;

				setTimeout(() => {
					timeStamp = 1; // again active
					}, ms)
				}
			}
		}
	};

window.addEventListener('load', function() {
	let carousel = new Carousel('#carousel');

	console.dir(carousel);
});

})();
